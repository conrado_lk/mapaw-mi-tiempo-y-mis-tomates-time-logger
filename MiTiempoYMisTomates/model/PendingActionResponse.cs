﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiTiempoYMisTomates.model
{
    class TaskStateResponse
    {
        public bool success { get; set; }
        public string message { get; set; }
        public string state { get; set; }
    }
}
