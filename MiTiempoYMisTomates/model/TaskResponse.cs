﻿namespace MiTiempoYMisTomates.model
{
    class TaskResponse
    {
        public bool success { get; set; }
        public string message { get; set; }
        public int taskId { get; set; }
        public string taskName { get; set; }
    }
}
