﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiTiempoYMisTomates.model
{
    public class Task
    {
        public int id { get; set; }
        public string name { get; set; }

        public Task (int taskId, string taskName)
        {
            id = taskId;
            name = taskName;
        }
    }
}
