﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiTiempoYMisTomates.model
{
    class RunningProgram
    {
        private string name { get; set; }
        private int runningSeconds { get; set; }

        public RunningProgram (string programName)
        {
            name = programName;
            runningSeconds = 0;
        }
    }
}
