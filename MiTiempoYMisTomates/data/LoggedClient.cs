﻿using MiTiempoYMisTomates.model;

namespace MiTiempoYMisTomates.data
{
    class LoggedClient
    {
        private static bool loggingTime = false;
        public static Account account { get; set; }
        public static Task currentTask { get; set; } = null;
        public static bool savingTimes { get; set; } = false;

        public static void EnableTimeLogging ()
        {
            loggingTime = true;
        }
        public static void DisableTimeLogging ()
        {
            loggingTime = false;
        }
        public static bool IsLoggingTime ()
        {
            return loggingTime;
        }
        public static void ResetCurrentTask ()
        {
            currentTask = null;
        } 
        public static void SetCurrentTaskData (Task task)
        {
            currentTask = task;
        }
    }
}
