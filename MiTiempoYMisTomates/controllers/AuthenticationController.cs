﻿using System;
using System.Threading.Tasks;
using Flurl.Http;
using Newtonsoft.Json;
using MiTiempoYMisTomates.model;
using MiTiempoYMisTomates.model.forms;

namespace MiTiempoYMisTomates.controllers
{
    class AuthenticationController
    {
        public static async Task<object> Authenticate(String emailParam, String passwordParam)
        {
            GenericResponseStatus authenticationResponseStatus = null;
            Account account = null;
            var url = "http://127.0.0.1:8000/auth";

            var jsonResponse = await url
                .PostUrlEncodedAsync(new { email = emailParam, password = passwordParam })
                .ReceiveString();

            account = JsonConvert.DeserializeObject<Account>(jsonResponse);
            authenticationResponseStatus = JsonConvert.DeserializeObject<GenericResponseStatus>(jsonResponse);

            var result = new { status = authenticationResponseStatus, accountData = account };

            return result;
        }
    }
}
