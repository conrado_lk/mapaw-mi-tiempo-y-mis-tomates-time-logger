﻿using System;
using System.Runtime.InteropServices;
using System.Diagnostics;
using MiTiempoYMisTomates.model;
using MiTiempoYMisTomates.data;
using Flurl.Http;
using Newtonsoft.Json;
using System.Windows.Forms;
using MiTiempoYMisTomates.controllers;
using System.Collections.Generic;
using System.Text;

namespace MiTiempoYMisTomates.controllers
{
    public class TimeLoggerController
    {
        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll")]
        private static extern Int32 GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);

        [DllImport("user32.dll")]
        static extern int GetWindowText(IntPtr hWnd, StringBuilder text, int count);

        string[] relevantProgramNames = {
            "Visual Studio Code",
            "Spotify",
            "Google Chrome",
            "Microsoft Edge",
            "Firefox",
            "HandBrake",
            "PhpStorm",
            "Microsoft Visual Studio 2017",
            "Pharo"
        };

        string[] relevantWebsiteNames =
        {
            "Twitter",
            "Facebook",
            "Instagram",
            "Gmail",
            "GitHub",
            "Wikipedia",
            "OLX",
            "Clarín",
            "Pagina12",
            "Diario El Día",
            "WhatsApp",
            "Amazon",
            "Mercado Libre",
            "Google Search"
        };

        Chrome chromeUtils = new Chrome();

        private static Process GetProcessByHandle(IntPtr hwnd)
        {
            try
            {
                uint processID;
                GetWindowThreadProcessId(hwnd, out processID);
                return Process.GetProcessById((int)processID);
            }
            catch { return null; }
        }

        private static Process GetActiveProcess()
        {
            IntPtr hwnd = GetForegroundWindow();
            return hwnd != null ? GetProcessByHandle(hwnd) : null;
        }

        public string CheckRelevantTitle(string titleApp)
        {
            foreach (string relevantWebsiteName in relevantWebsiteNames)
            {
                if (titleApp.Contains(relevantWebsiteName))
                    return relevantWebsiteName;
            }
            
            return "";
        }

        public bool CheckRelevantProgram(string programName)
        {
            foreach (string relevantProgramName in relevantProgramNames)
            {
                if (relevantProgramName == programName)
                    return true;
            }

            return false;
        }

        public void LogToDebugFile (string line)
        {
            using (System.IO.StreamWriter file =
        new System.IO.StreamWriter(@"debug.txt", true))
            {
                file.WriteLine($"[{ DateTime.Now.ToString("HH: mm:ss") }] { line} ");
            }
        }

        public void LogToProgramsFile(string line)
        {
            using (System.IO.StreamWriter file =
        new System.IO.StreamWriter(@"programs.txt", true))
            {
                file.WriteLine($"[{ DateTime.Now.ToString("HH: mm:ss") }] { line} ");
            }
        }

        private string GetActiveWindowTitle()
        {
            const int nChars = 256;
            StringBuilder Buff = new StringBuilder(nChars);
            IntPtr handle = GetForegroundWindow();

            if (GetWindowText(handle, Buff, nChars) > 0)
            {
                return Buff.ToString();
            }
            return null;
        }

        public void SaveProgramData(Process process)
        {
            try
            {
                string processName = process.MainModule.FileVersionInfo.FileDescription;

                Console.WriteLine("titleeeee" + process.MainWindowTitle);
                string applicationName = "";
                bool processApplicationName = CheckRelevantProgram(processName);
                string titleApplicationName = CheckRelevantTitle(process.MainWindowTitle);

                if (processApplicationName)
                {
                    applicationName = processName;
                } else if (titleApplicationName != "")
                {
                    applicationName = titleApplicationName;
                }
                            
                if (applicationName != "")
                {
                    try
                    {
                        if (titleApplicationName != "")
                        {
                            RunningProgramsStorage.storedPrograms.Add(titleApplicationName, 1);
                        }
                        if (processApplicationName)
                        {
                            RunningProgramsStorage.storedPrograms.Add(processName, 1);
                        }
                    }
                    catch (ArgumentException)
                    {
                        if (titleApplicationName != "")
                        {
                            RunningProgramsStorage.storedPrograms[titleApplicationName] += 1;
                        }
                        if (processApplicationName)
                        {
                            RunningProgramsStorage.storedPrograms[processName] += 1;

                        }
                    }
                    Console.WriteLine(process.MainWindowTitle);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

     

        public async void SendStoredProgramsAsync()
        {
            LogToDebugFile("SendStoredProgramsAsync START");
         
            if (LoggedClient.savingTimes)
                return;

            LogToDebugFile("SendStoredProgramsAsync pass savingTimes");


            LoggedClient.savingTimes = true;

            Console.WriteLine("SendStoredPrograms");

            GenericResponseStatus responseStatus = null;
            string JSONPrograms = JsonConvert.SerializeObject(RunningProgramsStorage.storedPrograms, Formatting.Indented);

            while (responseStatus == null || !responseStatus.success)
            {
                LogToDebugFile("SendStoredProgramsAsync pass while");

                Console.WriteLine("SendStoredPrograms IN");

                var url = "http://127.0.0.1:8000/log-app-data";
                LogToDebugFile("SendStoredProgramsAsync pass JSONPrograms");
                LogToDebugFile(JSONPrograms);

                try
                {
                    var requestData = new { userId = LoggedClient.account.id, programs = JSONPrograms };
                    var jsonResponse = await url
                            .PostUrlEncodedAsync(requestData)
                            .ReceiveString();
                    LogToDebugFile("SendStoredProgramsAsync pass jsonResponse");

                    responseStatus = JsonConvert.DeserializeObject<GenericResponseStatus>(jsonResponse);
                    LogToDebugFile("SendStoredProgramsAsync pass responseStatus deserialize");

                }
                catch (FlurlHttpException)
                {
                    continue;
                }
            }

            if(responseStatus.success)
            {
                LogToDebugFile("SendStoredProgramsAsync pass responseStatus.success");
                LogToProgramsFile(JSONPrograms);
                ResetStoredPrograms();
                LoggedClient.savingTimes = false;
            }
        }

        public void RunTimeLogger(Object source, System.Timers.ElapsedEventArgs e)
        {
            if (LoggedClient.IsLoggingTime())
            {
                Console.WriteLine("RunTimeLogger");
                Process currentProcess = GetActiveProcess();
                if (currentProcess != null)
                    SaveProgramData(currentProcess);
            }
        }

        public async void PollingAskForTask(Object source, System.Timers.ElapsedEventArgs e)
        {
            if (LoggedClient.currentTask != null)
                return;

            Console.WriteLine("Ask for Task Polling");

            TaskResponse response = null;
            string url = $"http://localhost:8000/ask-task/{ LoggedClient.account.id} ";

            try
            {
                response = await url.GetJsonAsync<TaskResponse>();
            }
            catch (FlurlHttpException)
            {
                return;
            }

            if (response.success)
            {
                Task task = new Task(response.taskId, response.taskName);
                LoggedClient.SetCurrentTaskData(task);
            }
        }


        public async void PollingTaskState (Object source, System.Timers.ElapsedEventArgs e)
        {
            if (LoggedClient.currentTask == null)
                return;

            Console.WriteLine("Polling Task State");

            TaskStateResponse response = null;
            string url = $"http://127.0.0.1:8000/check-task-state/{ LoggedClient.currentTask.id }";

            try
            {
                response = await url.GetJsonAsync<TaskStateResponse>();
            } catch (FlurlHttpException)
            {
                return;
            }

            if (response.success)
            {
                switch(response.state)
                {
                    case "ACTIVE":
                        LoggedClient.EnableTimeLogging();
                        LogToDebugFile("PollingTaskState ACTIVE");
                        break;
                    case "PENDING":
                    case "FINISHED":
                        LogToDebugFile("PollingTaskState PENDING/FINISHED");
                        LoggedClient.DisableTimeLogging();
                        LoggedClient.ResetCurrentTask();
                        SendStoredProgramsAsync();
                        break;
                }
            }
        }

        public static void ResetStoredPrograms()
        {
            RunningProgramsStorage.storedPrograms.Clear();
        }

    }
}
