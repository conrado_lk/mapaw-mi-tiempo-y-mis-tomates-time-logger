﻿using System;
using System.Windows.Forms;
using MiTiempoYMisTomates.controllers;
using MiTiempoYMisTomates.model.forms;
using MiTiempoYMisTomates.data;
using MiTiempoYMisTomates.forms;
using Flurl.Http;

namespace MiTiempoYMisTomates
{
    public partial class AuthenticationForm : Form
    {
        public AuthenticationForm()
        {
            InitializeComponent();
        }

        private void AuthenticationForm_Load(object sender, EventArgs e)
        {

        }

        private async void activate_Click(object sender, EventArgs e)
        {
            progressBar1.Value = 0;

            if (this.errorMessage.Visible)
            {
                this.errorMessage.Visible = false;
            }

            if (String.IsNullOrWhiteSpace(this.emailField.Text))
            {
                this.emailErrorMessage.Text = "El campo es obligatorio";
                this.emailErrorMessage.Visible = true;
            }

            if (String.IsNullOrWhiteSpace(this.passwordField.Text))
            {
                this.passwordErrorMessage.Text = "El campo es obligatorio";
                this.passwordErrorMessage.Visible = true;
            }

            if (this.passwordErrorMessage.Visible || this.emailErrorMessage.Visible)
                return;

            progressBar1.Visible = true;

            this.passwordErrorMessage.Visible = false;
            this.emailErrorMessage.Visible = false;

            progressBar1.Visible = true;
            progressBar1.Value = 100;
            dynamic authenticationResult = null;

            try
            {
                authenticationResult = await AuthenticationController.Authenticate(emailField.Text, passwordField.Text);
            } catch (FlurlHttpException)
            {
                authenticationResult = new { status = new { success = false, message = "Se produjo un error con el servidor" } };
            }

            if (authenticationResult.status.success)
            {
                LoggedClient.account = authenticationResult.accountData;
                this.Visible = false;
                AuthenticationSuccessForm authForm = new AuthenticationSuccessForm();
                authForm.ShowDialog();
            } else
            {
                this.progressBar1.Visible = false;
                this.errorMessage.Text = authenticationResult.status.message;
                this.errorMessage.Visible = true;

            }
        }

        private void emailField_TextChanged(object sender, EventArgs e)
        {
            if (!this.emailErrorMessage.Visible)
                return;

            if (!String.IsNullOrWhiteSpace(((TextBox)sender).Text)) {
                this.emailErrorMessage.Visible = false;
            }
        }

        private void passwordField_TextChanged(object sender, EventArgs e)
        {
            if (!this.passwordErrorMessage.Visible)
                return;

            if (!String.IsNullOrWhiteSpace(((TextBox)sender).Text))
            {
                this.passwordErrorMessage.Visible = false;
            }
        }

        private void emailField_Leave(object sender, System.EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(this.emailField.Text))
            {
                this.emailErrorMessage.Text = "El campo es obligatorio";
                this.emailErrorMessage.Visible = true;
            }
        }

        private void passwordField_Leave(object sender, System.EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(this.passwordField.Text))
            {
                this.passwordErrorMessage.Text = "El campo es obligatorio";
                this.passwordErrorMessage.Visible = true;
            }
        }

    }
}
