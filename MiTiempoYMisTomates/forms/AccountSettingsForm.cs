﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiTiempoYMisTomates.data;

namespace MiTiempoYMisTomates.forms
{
    public partial class AccountSettingsForm : Form
    {
        public AccountSettingsForm()
        {
            InitializeComponent();
        }

        private void AccountSettingsForm_Load(object sender, EventArgs e)
        {
            this.username.Text = $"{LoggedClient.account.first_name} {LoggedClient.account.last_name}";
            this.email.Text = LoggedClient.account.email;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult mBoxResult = MessageBox.Show("La aplicación se cerrará y no se registrarán los tiempos de uso ¿Está seguro?", "Cerrar", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            switch (mBoxResult)
            {
                case DialogResult.Yes:
                    Application.Exit();
                    break;
                case DialogResult.No:
                    break;
            }
        }
    }
}
