﻿using MiTiempoYMisTomates.forms;
using System;
using System.Windows.Forms;

namespace MiTiempoYMisTomates.model.forms
{
    partial class AuthenticationSuccessForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private static void exitApp(object sender, EventArgs e)
        {
            DialogResult mBoxResult = MessageBox.Show("La aplicación se cerrará y no se registrarán los tiempos de uso ¿Está seguro?", "Cerrar Mi Tiempo y Mis Tomates", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            switch (mBoxResult)
            {
                case DialogResult.Yes:
                    Application.Exit();
                    break;
                case DialogResult.No:
                    break;
            }
        }

        private static void appSettings(object sender, EventArgs e)
        {
            AccountSettingsForm accountSettingsForm =  new AccountSettingsForm();
            accountSettingsForm.Show();
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AuthenticationSuccessForm));
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.congratulationsMessage = new System.Windows.Forms.Label();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(447, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Desde ahora el tiempo de uso de sus aplicaciones será registrado.";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(101, 117);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(269, 33);
            this.button1.TabIndex = 1;
            this.button1.Text = "Cerrar ventana y registrar tiempos";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // congratulationsMessage
            // 
            this.congratulationsMessage.AutoEllipsis = true;
            this.congratulationsMessage.Dock = System.Windows.Forms.DockStyle.Top;
            this.congratulationsMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.congratulationsMessage.Location = new System.Drawing.Point(0, 0);
            this.congratulationsMessage.Name = "congratulationsMessage";
            this.congratulationsMessage.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.congratulationsMessage.Size = new System.Drawing.Size(468, 44);
            this.congratulationsMessage.TabIndex = 2;
            this.congratulationsMessage.Text = "congratulations";
            this.congratulationsMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon1.BalloonTipText = "Widget";
            this.notifyIcon1.BalloonTipTitle = "Widget";
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "Mi Tiempo Y Mis Tomates";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(416, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "Para realizar ajustes utilice el widget rojo en la barra de tareas.";
            // 
            // AuthenticationSuccessForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(468, 167);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.congratulationsMessage);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "AuthenticationSuccessForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mi Tiempo y Mis Tomates - Activación exitosa";
            this.Load += new System.EventHandler(this.AuthenticationSuccessForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label congratulationsMessage;
        private Label label2;
        public NotifyIcon notifyIcon1;
    }
}