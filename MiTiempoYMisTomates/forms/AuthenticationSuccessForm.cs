﻿using MiTiempoYMisTomates.forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiTiempoYMisTomates.controllers;

namespace MiTiempoYMisTomates.model.forms
{
    public partial class AuthenticationSuccessForm : Form
    {
        public TimeLoggerController timeLoggerController;
        public System.Timers.Timer getRunningProgramsTimer;
        public System.Timers.Timer pollingTimer;
        public System.Timers.Timer askForTaskTimer;

        public AuthenticationSuccessForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private static void showAppSettings(object sender, EventArgs e)
        {
            AccountSettingsForm accountSettingsForm = new AccountSettingsForm();
            accountSettingsForm.Show();
        }

        private void AuthenticationSuccessForm_Load(object sender, EventArgs e)
        {
            this.congratulationsMessage.Text = $"¡La activación fue exitosa {data.LoggedClient.account.first_name} {data.LoggedClient.account.last_name}!";

            MenuItem exitMenuItem = new MenuItem("Cerrar y dejar de registrar", exitApp);
            MenuItem settingsMenuItem = new MenuItem("Ajustes", showAppSettings);
            this.notifyIcon1.ContextMenu = new ContextMenu(new MenuItem[] { settingsMenuItem, exitMenuItem });
            
            timeLoggerController = new TimeLoggerController();

            getRunningProgramsTimer = new System.Timers.Timer(1000);
            getRunningProgramsTimer.Enabled = true;
            getRunningProgramsTimer.AutoReset = true;
            getRunningProgramsTimer.Elapsed += new System.Timers.ElapsedEventHandler(timeLoggerController.RunTimeLogger);
            getRunningProgramsTimer.Start();

            pollingTimer = new System.Timers.Timer(1500);
            pollingTimer.Enabled = true;
            pollingTimer.AutoReset = true;
            pollingTimer.Elapsed += new System.Timers.ElapsedEventHandler(timeLoggerController.PollingTaskState);
            pollingTimer.Start();

            askForTaskTimer = new System.Timers.Timer(1500);
            askForTaskTimer.Enabled = true;
            askForTaskTimer.AutoReset = true;
            askForTaskTimer.Elapsed += new System.Timers.ElapsedEventHandler(timeLoggerController.PollingAskForTask);
            askForTaskTimer.Start();
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            AccountSettingsForm accountSettingsForm = new AccountSettingsForm();
            accountSettingsForm.ShowDialog();
        }
    }
}
